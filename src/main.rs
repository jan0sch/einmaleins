///
/// Copyright (C) 2019  Jens Grassel
///
///    This program is free software: you can redistribute it and/or modify
///    it under the terms of the GNU Affero General Public License as published by
///    the Free Software Foundation, either version 3 of the License, or
///    (at your option) any later version.
///
///    This program is distributed in the hope that it will be useful,
///    but WITHOUT ANY WARRANTY; without even the implied warranty of
///    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///    GNU Affero General Public License for more details.
///
///    You should have received a copy of the GNU Affero General Public License
///    along with this program.  If not, see <https://www.gnu.org/licenses/>.
///
extern crate einmaleins;
extern crate rand;

use einmaleins::*;
use rand::prelude::*;
use std::process;

/// Übungen zum Einmaleins.
///
/// Es wird eine angegebene Anzahl von Aufgaben im Bereich des kleinen 
/// Einmaleins abgefragt wobei zwischen Multiplikation und Division 
/// gewechselt wird.
fn main() {
    print_titel();
    let mut r_mode: Option<Mode> = None;
    while r_mode.is_none() {
        r_mode = read_mode();
    }
    let mode = r_mode.unwrap();

    match mode {
        Mode::EXIT  => process::exit(0),
        _ => ()
    }

    let min = match mode {
        Mode::RANGE => {
            let x = read_num(Some("Startzahl (1): ".to_string()));
            if x > 0 && x < 21 {
                x
            } else {
                1
            }
        },
        _ => 1
    };
    let max = match mode {
        Mode::RANGE => {
            let x = read_num(Some("Endzahl (10): ".to_string()));
            if x > min && x < 21 {
                x
            } else if min > 10 {
                min + 1
            } else {
                10
            }
        },
        _ => 10
    };
    let max_num = read_num(Some("Wieviel Aufgaben?".to_string()));

    let mut s: Status = Status {
        erledigt: 0,
        falsch: 0,
        max: max_num,
    };

    while !s.fertig() {
        let a: u32 = rand::thread_rng().gen_range(min, max + 1);
        let b: u32 = rand::thread_rng().gen_range(min, max + 1);

        let op = mal_oder_durch();
        print_aufgabe(&op, a, b);
        let x = read_num(None);

        let (s2, ck) = check(s, a, b, op, x);
        if x == ck {
            println!("Richtig!");
        } else {
            println!("Falsch, richtig wäre {}.", ck);
        }
        s = s2;
    }

    abschluss(s);
}

fn print_titel() {
    println!("Übungen zum Einmaleins");
    println!("======================");
    println!("Bitte wähle die Art der Aufgaben (Taste):");
    println!("(e)inen Bereich angeben");
    println!("(z)ufällige Zahlen nehmen");
    println!("");
    println!("Um das Programm zu verlassen kannst Du x drücken.");
}

