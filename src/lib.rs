///
/// Copyright (C) 2019  Jens Grassel
///
///    This program is free software: you can redistribute it and/or modify
///    it under the terms of the GNU Affero General Public License as published by
///    the Free Software Foundation, either version 3 of the License, or
///    (at your option) any later version.
///
///    This program is distributed in the hope that it will be useful,
///    but WITHOUT ANY WARRANTY; without even the implied warranty of
///    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///    GNU Affero General Public License for more details.
///
///    You should have received a copy of the GNU Affero General Public License
///    along with this program.  If not, see <https://www.gnu.org/licenses/>.
///
extern crate rand;
extern crate termios;

use rand::prelude::*;
use std::io;
use std::io::Read;
use std::io::Write;
use termios::{Termios, TCSANOW, ECHO, ICANON, tcsetattr};

/// Ein Datentyp für den derzeitigen Status.
pub struct Status {
    pub erledigt: u32,
    pub falsch: u32,
    pub max: u32,
}

/// Methoden für den Status.
impl Status {
    /// Gibt an, ob wir fertig sind.
    pub fn fertig(&self) -> bool {
        self.erledigt >= self.max
    }
}

/// Ein Datentyp für die möglichen Rechenarten.
pub enum Operation {
    DIV,
    MUL,
}

/// Ein Datentyp für den Aufgabenmodus.
#[derive(PartialEq)]
pub enum Mode {
    EXIT,
    RANDOM,
    RANGE,
}

impl Mode {
    /// Wandelt die übergebene Zeichenkette in den entsprechenden Modus um.
    /// Ist keine Zuordnung möglich, wird `None` zurückgegeben.
    pub fn from_string(s: String) -> Option<Mode> {
        match s.to_lowercase().as_ref() {
            "e" => Some(Mode::RANGE),
            "x" => Some(Mode::EXIT),
            "z" => Some(Mode::RANDOM),
            _   => None
        }
    }
}

/// Zufällig MUL oder DIV zurückgeben.
pub fn mal_oder_durch() -> Operation {
    if random() {
        Operation::MUL
    } else {
        Operation::DIV
    }
}

/// Zähle die falschen Antworten hoch.
fn ist_falsch(s: Status) -> Status {
    Status {
        falsch: s.falsch + 1,
        ..s
    }
}

/// Zähle die richtigen Antworten hoch.
fn ist_richtig(s: Status) -> Status {
    Status {
        erledigt: s.erledigt + 1,
        ..s
    }
}

/// Prüft die gegebene Antwort anhand der vorhandenen Parameter.
pub fn check(s: Status, x: u32, y: u32, op: Operation, antwort: u32) -> (Status, u32) {
    if s.fertig() {
        (s, antwort)
    } else {
        match op {
            Operation::DIV =>
                if antwort == y {
                    (ist_richtig(s), y)
                } else {
                    (ist_falsch(s), y)
                }
            Operation::MUL =>
                if antwort == x * y {
                    (ist_richtig(s), x * y)
                } else {
                    (ist_falsch(s), x * y)
                }
        }
    }
}


/// Die Aufgabe ausgeben.
pub fn print_aufgabe(op: &Operation, x: u32, y: u32) {
    match op {
        Operation::MUL => println!("{} * {} = ?", x, y),
        Operation::DIV => println!("{} : {} = ?", x * y, x),
    }
}

/// Bericht zum Abschluß
pub fn abschluss(s: Status) {
    println!("Du bist fertig und hattest {} Aufgaben richtig und {} falsch.", s.erledigt, s.falsch);
    let r = s.erledigt as f32;
    let f = s.falsch as f32;
    let p = 100.0 * f / (r + f);
    if p as u32 == 0 {
        println!("Herzlichen Glückwunsch! Du hattest alles richtig.");
        print_smiley();
    } else if p as u32 <= 10 {
        println!("Du hattest fast alles richtig. Sehr gut, weiter so!");
    } else {
        println!("Bleib dran, Übung macht den Meister! :-)")
    }
}

pub fn read_mode() -> Option<Mode> {
    let stdin = 0;
    let term = Termios::from_fd(stdin).unwrap();
    let mut t = term.clone();
    t.c_lflag &= !(ICANON | ECHO);
    tcsetattr(stdin, TCSANOW, &mut t).unwrap();
    let stdout = io::stdout();
    let mut reader = io::stdin();
    let mut buffer = [0;1];
    stdout.lock().flush().unwrap();
    reader.read_exact(&mut buffer).unwrap();
    tcsetattr(stdin, TCSANOW, & term).unwrap();
    match String::from_utf8(buffer.to_vec()) {
        Ok(s)   => Mode::from_string(s),
        Err(..) => None
    }
}

/// Liest von der Standardeingabe und gibt eine Zahl zurück.
/// Im Fehlerfall wird eine `0` zurückgegeben.
pub fn read_num(msg: Option<String>) -> u32 {
    match msg {
        Some(str) => println!("{}", str),
        None      => ()
    }
    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Eingabefehler!");
    //let num: u32 = input.trim().parse().expect("Keine Zahl!");
    //num
    match input.trim().parse() {
        Ok(num) => num,
        Err(_)  => 0,
    }
}

/// Zeichnet einen Smiley... =)
fn print_smiley() {
    println!("      ..::''''::..");
    println!("    .;''        ``;.");
    println!("   ::    ::  ::    ::");
    println!("  ::     ::  ::     ::");
    println!("  :: .:' ::  :: `:. ::");
    println!("  ::  :          :  ::");
    println!("   :: `:.      .:' ::");
    println!("    `;..``::::''..;'");
    println!("      ``::,,,,::''");
}

///
/// Unit Tests
///
#[cfg(test)]
extern crate quickcheck;
#[cfg(test)]
#[macro_use(quickcheck)]
extern crate quickcheck_macros;

#[cfg(test)]
mod tests {
    use super::*;

    #[quickcheck]
    fn status_erledigt_prop(e: u32, f: u32, m: u32) -> bool {
        let s: Status = Status {
            erledigt: e,
            falsch: f,
            max: m,
        };
        let f = s.erledigt >= s.max;
        s.fertig() == f
    }

    #[quickcheck]
    fn mode_from_string_prop(s: String) -> bool {
        let x = s.to_lowercase();
        match x.as_ref() {
            "e" => Mode::from_string(s) == Some(Mode::RANGE),
            "x" => Mode::from_string(s) == Some(Mode::EXIT),
            "z" => Mode::from_string(s) == Some(Mode::RANDOM),
            _   => Mode::from_string(s) == None
        }
    }

    #[test]
    fn mode_from_string_range() {
        assert!(Mode::from_string(String::from("e")) == Some(Mode::RANGE))
    }

    #[test]
    fn mode_from_string_exit() {
        assert!(Mode::from_string(String::from("x")) == Some(Mode::EXIT))
    }

    #[test]
    fn mode_from_string_random() {
        assert!(Mode::from_string(String::from("z")) == Some(Mode::RANDOM))
    }

    #[test]
    fn status_erledigt() {
        let s: Status = Status {
            erledigt: 1,
            falsch: 1,
            max: 1,
        };
        assert!(s.fertig());
    }
}
